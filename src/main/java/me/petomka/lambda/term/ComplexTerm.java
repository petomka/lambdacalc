package me.petomka.lambda.term;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(of = {"t1", "t2"}, callSuper = false)
public class ComplexTerm extends Term {

	final Term t1;
	final Term t2;

	public ComplexTerm(Term t1, Term t2) {
		this.t1 = t1;
		this.t2 = t2;
	}

	@Override
	public Set<Variable> getFreeVariables() {
		Set<Variable> fv = new HashSet<>(t1.getFreeVariables());
		fv.addAll(t2.getFreeVariables());
		return fv;
	}

	@Override
	public Term betaReduce() {
		if (t1 instanceof Lambda) {
			Lambda l = (Lambda) t1;
			Substitution sigma = new Substitution(l.boundVariable, t2);
			return sigma.substitute(l.betaRedex);
		} else {
			return new ComplexTerm(t1.betaReduce(), t2.betaReduce());
		}
	}

	@Override
	public String toString() {
		return "(" + t1.toString() + " " + t2.toString() + ")";
	}

}
