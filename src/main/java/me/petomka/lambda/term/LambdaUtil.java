package me.petomka.lambda.term;

public class LambdaUtil {

	public static String toPrettyString(Term term) {
		if (term instanceof Lambda) {
			return "(λ" + getConsecutiveLambdaVars((Lambda) term) + "." + toPrettyStringOmitLambda(((Lambda) term).betaRedex) + ")";
		}
		if (term instanceof Variable) {
			return term.toString();
		}
		if (term instanceof ComplexTerm) {
			Term t = ((ComplexTerm) term).t1;
			Term s = ((ComplexTerm) term).t2;
			String ts = toPrettyString(t);
			String ss;
			if (s instanceof ComplexTerm) {
				String s1 = toPrettyString(((ComplexTerm) s).t1);
				String s2 = toPrettyString(((ComplexTerm) s).t2);
				ss = "(" + s1 + "" + s2 + ")";
			} else {
				ss = toPrettyString(s);
			}
			return ts + ss;
		}
		throw new IllegalArgumentException("Cannot beautify term of unknown type. Term = " + term);
	}

	private static String toPrettyStringOmitLambda(Term term) {
		if (term instanceof Lambda) {
			return toPrettyStringOmitLambda(((Lambda) term).betaRedex);
		}
		return toPrettyString(term);
	}

	private static String getConsecutiveLambdaVars(Lambda lambda) {
		String vars = lambda.boundVariable.toString();
		if (lambda.betaRedex instanceof Lambda) {
			vars += getConsecutiveLambdaVars((Lambda) lambda.betaRedex);
		}
		return vars;
	}


}
