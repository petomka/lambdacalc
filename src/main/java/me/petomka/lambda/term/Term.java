package me.petomka.lambda.term;

import java.util.Set;

public abstract class Term {

	public abstract Set<Variable> getFreeVariables();

	public abstract Term betaReduce();

}
