package me.petomka.lambda.term;

import java.util.Set;
import java.util.stream.IntStream;

public class LambdaParser {

	private static final Set<String> lambdas = Set.of(
			"lambda",
			"λ"
	);

	public static Term parse(String input) {
		if (input == null || input.isEmpty()) {
			return null;
		}
		for (String lambdaVal : lambdas) {
			if (input.startsWith(lambdaVal)) {
				String args = input.substring(lambdaVal.length(), input.indexOf("."));
				String rest = input.substring(input.indexOf(".") + 1);
				Term lambdaArgs = parse(args);
				Lambda result = null;
				while (lambdaArgs instanceof ComplexTerm) {
					Term currentArg = ((ComplexTerm) lambdaArgs).t2;
					if (!(currentArg instanceof Variable)) {
						throw new IllegalArgumentException("Non variable in lambda argument");
					}
					if (result == null) {
						result = new Lambda((Variable) currentArg, parse(rest));
					} else {
						result = new Lambda((Variable) currentArg, result);
					}
					lambdaArgs = ((ComplexTerm) lambdaArgs).t1;
				}
				if (!(lambdaArgs instanceof Variable)) {
					throw new IllegalArgumentException("Non variable in lambda argument");
				}
				if (result == null) {
					result = new Lambda((Variable) lambdaArgs, parse(rest));
				} else {
					result = new Lambda((Variable) lambdaArgs, result);
				}
				return result;
			}
		}
		boolean enclosed = findClosingBracketIndex(0, input) == input.length() - 1;
		if (!enclosed && (input.contains(" ") || input.contains("("))) {
			String[] parts = splitAtLastPossibleSpace(input);
			Term left = parse(parts[0]);
			Term right = parse(parts[1]);
			if (left != null && right != null) {
				return new ComplexTerm(left, right);
			}
			if (right != null) {
				return right;
			}
			return left;
		}
		if (input.startsWith("(") || enclosed) {
			int bracketIndex = findClosingBracketIndex(0, input);
			Term left = parse(input.substring(1, bracketIndex));
			Term right = parse(input.substring(bracketIndex + 1));
			if (right != null) {
				return new ComplexTerm(left, right);
			}
			return left;
		}
		return new Variable(input);
	}

	private static int findClosingBracketIndex(int startIndex, String input) {
		if(input.lastIndexOf(')') == -1) {
			return -1;
		}
		int score = 0;
		int index = startIndex;
		do {
			char c = input.charAt(index);
			if (c == '(') {
				score += 1;
			} else if (c == ')') {
				score -= 1;
			}
			index++;
		} while (score != 0 && index < input.length());
		if (score != 0) {
			throw new IllegalArgumentException("Unclosed brackets for input \"" + input + "\"");
		}
		if (index == 0) {
			throw new IllegalArgumentException("Sorry, but I don't know what went wrong.");
		}
		return index - 1;
	}

	private static int[] getBracketScore(String input) {
		int[] result = new int[input.length()];
		int score = 0;
		int index = 0;
		do {
			char c = input.charAt(index);
			if (c == '(') {
				score += 1;
			} else if (c == ')') {
				score -= 1;
			}
			result[index] = score;
			index++;
		} while (index < input.length());
		return result;
	}

	private static String[] splitAtLastPossibleSpace(String input) {
		input = input.trim();
		if (input.isEmpty()) {
			return new String[]{"", ""};
		}
		int[] scores = getBracketScore(input);
		int min = IntStream.of(scores).min().orElse(-1);
		if (min == -1) {
			throw new IllegalArgumentException("Empty input string");
		}
		for (int index = input.length() - 2; index >= 0; index--) {
			if (scores[index] == min &&
					(input.charAt(index) == ' ' || input.charAt(index) == ')' || input.charAt(index + 1) == '(')) {
				String left = input.substring(0, index + 1);
				String right = input.substring(index + 1);
				return new String[]{left, right};
			}
		}
		return new String[]{input, ""};
	}

}
