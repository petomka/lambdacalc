package me.petomka.lambda.term;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(of = {"boundVariable", "betaRedex"}, callSuper = false)
public class Lambda extends Term {

	final Variable boundVariable;

	final Term betaRedex;

	public Lambda(Variable boundVariable, Term betaRedex) {
		this.boundVariable = boundVariable;
		this.betaRedex = betaRedex;
	}

	@Override
	public Set<Variable> getFreeVariables() {
		Set<Variable> freeVars = new HashSet<>(betaRedex.getFreeVariables());
		freeVars.remove(boundVariable);
		return freeVars;
	}

	@Override
	public Term betaReduce() {
		return new Lambda(boundVariable, betaRedex.betaReduce());
	}

	@Override
	public String toString() {
		return "(λ" + boundVariable.getName() + "." + betaRedex.toString() + ")";
	}

}
