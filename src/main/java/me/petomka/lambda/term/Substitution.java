package me.petomka.lambda.term;

import me.petomka.lambda.SubstitutionUtil;

import java.util.Set;
import java.util.stream.Collectors;

public class Substitution {

	private final Term toSubstitute;
	private final Term substitute;

	private Substitution then;

	public Substitution(Term toSubstitute, Term substitute) {
		this.toSubstitute = toSubstitute;
		this.substitute = substitute;
	}

	public Term getToSubstitute() {
		return toSubstitute;
	}

	public Term getSubstitute() {
		return substitute;
	}

	public Substitution then(Substitution sigma) {
		Substitution sigmaPrime;

		boolean sigmaUsed = false;

		if (this.getToSubstitute().equals(sigma.getToSubstitute())) {
			sigmaUsed = true;
			sigmaPrime = new Substitution(sigma.getToSubstitute(), sigma.getSubstitute());
		} else {
			sigmaPrime = new Substitution(getToSubstitute(), getSubstitute());
		}

		Substitution drag = this;
		Substitution current = sigmaPrime;
		while (drag.then != null) {
			if (!sigmaUsed && drag.then.getToSubstitute().equals(sigma.getToSubstitute())) {
				current.then = new Substitution(sigma.getToSubstitute(), sigma.getSubstitute());
				sigmaUsed = true;
			} else {
				current.then = new Substitution(drag.then.getToSubstitute(), drag.then.getSubstitute());
			}
			current = current.then;
			drag = drag.then;
		}

		if (!sigmaUsed) {
			current.then = sigma;
		}

		return sigmaPrime;
	}

	public Term substitute(Variable x) {
		Substitution substitution = this;
		Term y;
		do {
			y = x.substitute(substitution);
		} while (y.equals(x) && (substitution = substitution.then) != null);
		return y;
	}

	public ComplexTerm substitute(ComplexTerm ts) {
		return new ComplexTerm(substitute(ts.t1), substitute(ts.t2));
	}

	public Lambda substitute(Lambda lambda) {
		Set<Variable> fvs = lambda.betaRedex.getFreeVariables();
		fvs = fvs.stream().map(this::substitute)
				.filter(term -> term instanceof Variable)
				.map(term -> (Variable) term)
				.collect(Collectors.toSet());
		Variable fresh = lambda.boundVariable;
		if (fvs.contains(fresh)) {
			fresh = SubstitutionUtil.getFreshVariable(fvs);
		}
		Substitution sigmaPrime = new Substitution(lambda.boundVariable, fresh);
		Term t = this.then(sigmaPrime).substitute(lambda.betaRedex);
		return new Lambda(fresh, t);
	}

	public Term substitute(Term term) {
		if (term instanceof Variable) {
			return substitute((Variable) term);
		}
		if (term instanceof ComplexTerm) {
			return substitute((ComplexTerm) term);
		}
		if (term instanceof Lambda) {
			return substitute((Lambda) term);
		}
		throw new IllegalArgumentException("Cannot substitute term of unknown type. Term = " + term);
	}

	public String toString() {
		return "σ[" + toStringCore() + "]";
	}

	private String toStringCore() {
		String s = toSubstitute.toString() + " -> " + substitute.toString();
		if (then != null) {
			s += ", " + then.toStringCore();
		}
		return s;
	}

}
