package me.petomka.lambda.term;

import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(of = {"name"}, callSuper = false)
public class Variable extends Term {

	private final String name;

	public Variable(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public Set<Variable> getFreeVariables() {
		return Set.of(this);
	}

	@Override
	public Term betaReduce() {
		return this;
	}

	public Term substitute(Substitution sigma) {
		if (sigma.getToSubstitute().equals(this)) {
			return sigma.getSubstitute();
		}
		return this;
	}

	@Override
	public String toString() {
		String name = getName();
		if (name.length() > 1) {
			return "(" + name + ")";
		}
		return name;
	}

}
