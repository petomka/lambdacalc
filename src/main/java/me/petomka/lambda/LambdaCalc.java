package me.petomka.lambda;

import me.petomka.lambda.term.LambdaParser;
import me.petomka.lambda.term.LambdaUtil;
import me.petomka.lambda.term.Term;

public class LambdaCalc {

	public static void main(String[] args) {
		Term term = LambdaParser.parse("(lambda x y . y x) a b");
		term = term.betaReduce().betaReduce(); // first reduce to (lambda y . y a) b, then reduce to b a
		System.out.println(LambdaUtil.toPrettyString(term)); // Pretty print - calling toString is also possible
	}

}


