package me.petomka.lambda;

import me.petomka.lambda.term.Variable;

import java.util.Set;

public class SubstitutionUtil {

	private static final char[] varChars = "abcdefghijklmnopqrstuvwxyz".toCharArray();

	public static Variable getFreshVariable(Set<Variable> toExclude) {
		int i = 0;
		while (true) {
			Variable v = new Variable(toString(i, varChars));
			if (!toExclude.contains(v)) {
				return v;
			}
			i++;
		}
	}

	private static String toString(int i, char[] chars) {
		StringBuilder s = new StringBuilder();
		if (i == 0) {
			s.append(chars[0]);
			return s.toString();
		}
		while (i > 0) {
			s.append(chars[i % chars.length]);
			i /= chars.length;
		}
		return s.toString();
	}

}
